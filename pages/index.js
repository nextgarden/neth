import Head from 'next/head'
import { Eth } from 'web3-eth';
import { Drizzle } from "@drizzle/store";
import LocalMessageDuplexStream from 'post-message-stream';
import SimpleStorage from "./contracts/SimpleStorage.json";
import MetaMaskOnboarding from '@metamask/onboarding';

const drizzleOptions = {
  contracts: [SimpleStorage],
  events: {
    SimpleStorage: ["StorageSet"],
  },
};


// Create a stream to a remote provider:
const metamaskStream = new LocalMessageDuplexStream({
  name: 'inpage',
  target: 'contentscript',
})

// this will initialize the provider and set it as window.ethereum
initializeProvider({
  connectionStream: metamaskStream,
})

const { ethereum } = window

const drizzle = new Drizzle(drizzleOptions);

export default function Home() {
  var eth = new Eth(ethereum);
  console.log(eth);
  const onboarding = new MetaMaskOnboarding();
  onboarding.startOnboarding();
  return  (
    <div>
      <Head>
        <title>My wallet accounts</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <p>Wallet info: { eth.getAccounts() } </p>
    </div>
  )
};
